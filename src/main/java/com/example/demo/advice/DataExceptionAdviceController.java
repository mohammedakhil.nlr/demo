package com.example.demo.advice;

import com.example.demo.exception.InValidDataException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Order(Ordered.LOWEST_PRECEDENCE - 200)
@ControllerAdvice
public class DataExceptionAdviceController {
    @ExceptionHandler(InValidDataException.class)
    @ResponseBody
    public ResponseEntity<String> handleOperationException(HttpSession session, InValidDataException exception) {
        return new ResponseEntity<String>(exception.getMessage(),  HttpStatus.BAD_REQUEST);
    }
}
