package com.example.demo.advice;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice
public class GeneralExceptionControllerAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<String> handleGeneralException(HttpSession session, Exception exception) {
/*

        String requestXml = (String)session.getAttribute("SESSION_ATTRIBUTE_REQUEST_STRING");
        log request for support team and debugging

*/
        return new ResponseEntity<String>("Unexpected exception: Contact HelpDesk",  HttpStatus.INTERNAL_SERVER_ERROR);

    }

}
