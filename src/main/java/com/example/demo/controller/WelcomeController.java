package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akmohammed on 11/18/2017.
 */
@RestController
public class WelcomeController {
    @RequestMapping(value = "/", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public ResponseEntity<String> welcome() {
        return new ResponseEntity<String>("Welcome", HttpStatus.OK);
    }
}
