package com.example.demo.controller.customer;

import com.example.demo.database.model.Customer;
import com.example.demo.database.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Set;

/**
 * Created by akmohammed on 11/18/2017.
 */
@RestController
public class CustomerController {
    private final CustomerService customerService;

    @Inject
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getCustomerById(@RequestParam("id") String id) {
        Set<Customer>customerSet = customerService.getAll();
        for (Customer customer:customerSet) {
            System.out.println(customer.toString());
        }
        Customer customer = customerService.findByCustomerId(id);
        return new ResponseEntity<String>(customer.toString(), HttpStatus.OK);
    }


    @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> add(@RequestBody String jsonInString) {
        Customer customer = customerService.add(jsonInString);
        return new ResponseEntity<String>(customer.toString(), HttpStatus.OK);
    }
}
