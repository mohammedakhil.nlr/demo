package com.example.demo.controller.claim;

import com.example.demo.database.model.Customer;
import com.example.demo.database.service.ClaimService;
import com.example.demo.exception.InValidDataException;
import com.example.demo.requests.ClaimRequests;
import com.example.demo.requests.ClaimResponse;
import com.example.demo.validator.ClaimValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

@RestController
public class ClaimController {
    private final ClaimService claimService;
    private final ClaimValidator claimValidator;

    @Inject
    public ClaimController(ClaimService claimService, ClaimValidator claimValidator) {
        this.claimService = claimService;
        this.claimValidator = claimValidator;
    }

    @RequestMapping(value = "/claims/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> add(HttpSession session,
                                      @RequestBody String jsonInString) {
        session.setAttribute("SESSION_ATTRIBUTE_REQUEST_API", "CLAIMS_ADD");
        session.setAttribute("SESSION_ATTRIBUTE_REQUEST_STRING", jsonInString);

        ObjectMapper objectMapper = new ObjectMapper();
        ClaimRequests obj = null;
        try {

            obj = objectMapper.readValue(jsonInString, ClaimRequests.class);
        } catch (IOException ioException) {
            throw new InValidDataException("unable to parse request");
        }

        claimValidator.validate(obj);
        claimService.add(obj);
        return new ResponseEntity<String>("Accepted for Validation", HttpStatus.OK);
    }

    @RequestMapping(value = "/claims", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getCustomerById(@RequestParam("claimNumber") String claimNumber,
                                                  @RequestParam("cust_id") String cust_id) {
        ClaimResponse claimResponse = claimService.getClaimResponse(claimNumber, cust_id);
        return new ResponseEntity<String>(claimResponse.toString(), HttpStatus.OK);
    }
}
