package com.example.demo.requests;

import java.io.Serializable;

/**
 * Created by akmohammed on 11/18/2017.
 */
public class ClaimResponseDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    private String partName;
    private String partImagePath;
    private String statusFlag;

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartImagePath() {
        return partImagePath;
    }

    public void setPartImagePath(String partImagePath) {
        this.partImagePath = partImagePath;
    }
}
