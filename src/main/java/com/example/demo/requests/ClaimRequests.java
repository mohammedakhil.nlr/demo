package com.example.demo.requests;

import java.io.Serializable;
import java.util.List;

/**
 * Created by akmohammed on 11/18/2017.
 */
public class ClaimRequests implements Serializable {
    private static final long serialVersionUID = 1L;
    private String claimNumber;
    private String customerId;
    private String claimedAmount;
    private List<ClaimRequestDetails> claimRequestDetails;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getClaimedAmount() {
        return claimedAmount;
    }

    public void setClaimedAmount(String claimedAmount) {
        this.claimedAmount = claimedAmount;
    }

    public List<ClaimRequestDetails> getClaimRequestDetails() {
        return claimRequestDetails;
    }

    public void setClaimRequestDetails(List<ClaimRequestDetails> claimRequestDetails) {
        this.claimRequestDetails = claimRequestDetails;
    }
}
