package com.example.demo.requests;

import java.io.Serializable;

/**
 * Created by akmohammed on 11/18/2017.
 */
public class ClaimRequestDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    private String partName;
    private String partImagePath;

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartImagePath() {
        return partImagePath;
    }

    public void setPartImagePath(String partImagePath) {
        this.partImagePath = partImagePath;
    }
}
