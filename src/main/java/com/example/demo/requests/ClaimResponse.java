package com.example.demo.requests;

import com.example.demo.database.model.Claims;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by akmohammed on 11/18/2017.
 */
public class ClaimResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private Claims claims;
    private List<ClaimResponseDetails> claimResponseDetailses;

    public Claims getClaims() {
        return claims;
    }

    public void setClaims(Claims claims) {
        this.claims = claims;
    }

    public List<ClaimResponseDetails> getClaimResponseDetailses() {
        return claimResponseDetailses;
    }

    public void setClaimResponseDetailses(List<ClaimResponseDetails> claimResponseDetailses) {
        this.claimResponseDetailses = claimResponseDetailses;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("claims", claims)
                .append("claimResponseDetailses", claimResponseDetailses)
                .toString();
    }
}
