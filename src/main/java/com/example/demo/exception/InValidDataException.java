package com.example.demo.exception;

/**
 * Created by akmohammed on 11/18/2017.
 */
public class InValidDataException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InValidDataException(String message) {
        super(message);
    }

    public InValidDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
