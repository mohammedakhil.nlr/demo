package com.example.demo.validator;

import com.example.demo.database.model.Customer;

/**
 * Created by akmohammed on 11/18/2017.
 */
public interface CustomerValidator {
    void validate(Customer customer);
}
