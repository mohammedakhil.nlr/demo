package com.example.demo.validator;

import com.example.demo.requests.ClaimRequests;

/**
 * Created by akmohammed on 11/18/2017.
 */
public interface ClaimValidator {
    void validate(ClaimRequests claimRequests);

}
