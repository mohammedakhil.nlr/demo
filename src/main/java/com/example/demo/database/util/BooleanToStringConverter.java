package com.example.demo.database.util;

import org.apache.commons.lang3.BooleanUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Converter
public class BooleanToStringConverter implements AttributeConverter<Boolean, String> {
    public String convertToDatabaseColumn(Boolean value) {
        return BooleanUtils.toString(value, "1", "0", "0");
    }

    public Boolean convertToEntityAttribute(String value) {
        return value != null && BooleanUtils.toBoolean(value, "1", "0");
    }
}
