package com.example.demo.database.service.impl;

import com.example.demo.database.model.ClaimDetails;
import com.example.demo.database.model.Claims;
import com.example.demo.database.model.Customer;
import com.example.demo.database.repository.ClaimDetailsRepository;
import com.example.demo.database.repository.ClaimsRepository;
import com.example.demo.database.repository.CustomerRepository;
import com.example.demo.database.service.ClaimService;
import com.example.demo.exception.InValidDataException;
import com.example.demo.requests.ClaimRequestDetails;
import com.example.demo.requests.ClaimRequests;
import com.example.demo.requests.ClaimResponse;
import com.example.demo.requests.ClaimResponseDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Service
public class ClaimServiceImpl implements ClaimService {
    private final ClaimsRepository claimsRepository;
    private final CustomerRepository customerRepository;
    private final ClaimDetailsRepository claimDetailsRepository;
    private static final AtomicInteger ID_COUNTER = new AtomicInteger(new Random().nextInt(999999));
    private static final AtomicInteger ID_COUNTER2 = new AtomicInteger(new Random().nextInt(999999));

    @Inject
    public ClaimServiceImpl(ClaimsRepository claimsRepository,
                            CustomerRepository customerRepository,
                            ClaimDetailsRepository claimDetailsRepository) {
        this.claimsRepository = claimsRepository;
        this.claimDetailsRepository = claimDetailsRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public void add(ClaimRequests obj) {
        ObjectMapper objectMapper = new ObjectMapper();
        Customer customer = customerRepository.findById(obj.getCustomerId());
        Claims claims = new Claims();
        claims.setClaimId(Integer.toString(ID_COUNTER.incrementAndGet()));
        claims.setClaimedAmount(Double.parseDouble(obj.getClaimedAmount()));
        claims.setClaimNumber(obj.getClaimNumber());
        claims.setCustomer(customer);
        claims.setClaimStatus("P");
        claimsRepository.saveAndFlush(claims);

        List<ClaimDetails> claimDetailsList;

        if(obj.getClaimRequestDetails()!=null && !obj.getClaimRequestDetails().isEmpty()) {
            claimDetailsList = new ArrayList<>();

            for(ClaimRequestDetails claimRequestDetails : obj.getClaimRequestDetails()) {
                ClaimDetails claimDetails = new ClaimDetails();
                claimDetails.setClaimDetailsId(Integer.toString(ID_COUNTER2.incrementAndGet()));
                claimDetails.setClaims(claims);
                claimDetails.setKeyPath(claimRequestDetails.getPartImagePath());
                claimDetails.setKeyType(claimRequestDetails.getPartName());
                claimDetails.setKeyStatusFlag("P");
                claimDetailsList.add(claimDetails);
            }
            claimDetailsRepository.save(claimDetailsList);
        }
    }

    @Override
    public ClaimResponse getClaimResponse(String claimNumber, String customerId) {
        Claims claims = claimsRepository.getClaimsByCustomerIdAndClaimNumber(customerId, claimNumber);
        ClaimResponse claimResponse = null;
        if(claims != null) {
            claimResponse = new ClaimResponse();
            claimResponse.setClaims(claims);
            List<ClaimDetails> claimDetails = claimDetailsRepository.getClaimsDetailsByClaimId(claims.getClaimId());
            if(claimDetails!=null && !claimDetails.isEmpty()) {
                claimResponse.setClaimResponseDetailses(new ArrayList<>());
                for (ClaimDetails claimDetails1:claimDetails) {
                    ClaimResponseDetails claimResponseDetails = new ClaimResponseDetails();
                    claimResponseDetails.setPartName(claimDetails1.getKeyType());
                    claimResponseDetails.setPartImagePath(claimDetails1.getKeyPath());
                    claimResponseDetails.setStatusFlag(claimDetails1.getKeyStatusFlag());
                    claimResponse.getClaimResponseDetailses().add(claimResponseDetails);
                }
            }
        }
        return claimResponse;
    }
}
