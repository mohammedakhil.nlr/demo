package com.example.demo.database.service;

import com.example.demo.requests.ClaimRequests;
import com.example.demo.requests.ClaimResponse;
import org.springframework.stereotype.Service;

public interface ClaimService {
    void add(ClaimRequests claimRequests);

    ClaimResponse getClaimResponse(String claimNumber, String customerId);
}
