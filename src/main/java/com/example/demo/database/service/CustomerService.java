package com.example.demo.database.service;

import com.example.demo.database.model.Customer;

import java.util.Set;

public interface CustomerService {
    Customer add(Customer customer);
    Customer add(String jsonString);

    Customer findByCustomerId(String customerId);

    Set<Customer> getAll();
}
