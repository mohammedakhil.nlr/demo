package com.example.demo.database.service.impl;

import com.example.demo.database.model.Customer;
import com.example.demo.database.repository.CustomerRepository;
import com.example.demo.database.service.CustomerService;
import com.example.demo.exception.InValidDataException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    @Inject
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer add(Customer customer) {
        return customerRepository.saveAndFlush(customer);
    }

    @Override
    public Customer findByCustomerId(String customerId) {
        return customerRepository.findById(customerId);
    }

    @Override
    public Customer add(String jsonString) {
        ObjectMapper objectMapper = new ObjectMapper();
        Customer obj;
        try {
            obj = objectMapper.readValue(jsonString, Customer.class);
        } catch (IOException ioException) {
            throw new InValidDataException("unable to parse request");
        }
        customerRepository.saveAndFlush(obj);

        return obj;
    }

    @Override
    public Set<Customer> getAll() {
        Set<Customer> customerSet = new HashSet<>(customerRepository.findAll());
        Set<Customer> immutableCutomers = Collections.unmodifiableSet(customerSet);
        return immutableCutomers;
    }
}
