package com.example.demo.database.repository;

import com.example.demo.database.model.ClaimDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Repository
public interface ClaimDetailsRepository extends JpaRepository<ClaimDetails, String> {
    @Query("from ClaimDetails " +
            "where claims.claimId=?1 ")
    List<ClaimDetails> getClaimsDetailsByClaimId(String claimId);

}
