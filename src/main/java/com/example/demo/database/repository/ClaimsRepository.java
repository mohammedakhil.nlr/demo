package com.example.demo.database.repository;

import com.example.demo.database.model.Claims;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Repository
public interface ClaimsRepository  extends JpaRepository<Claims, String> {
    @Query("from Claims " +
            "where customer.id=?1 AND claimNumber=?2 ")
    Claims getClaimsByCustomerIdAndClaimNumber(String customerId, String claimNumber);

    @Query("from Claims " +
            "where customer.id=?1 ")
    List<Claims> getClaimsByCustomerId(String customerId);
}
