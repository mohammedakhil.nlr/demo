package com.example.demo.database.repository;

import com.example.demo.database.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {
    Customer findByIdAndActiveFlag(String id, boolean activeFlag);

    Customer findById(String id);
}
