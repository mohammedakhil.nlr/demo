package com.example.demo.database.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "CLAIMS")
public class Claims implements Serializable {
    private static final long serialVersionUID = 1L;
    private String claimId;
    private String claimNumber;
    private double claimedAmount;
    private double sanctionAmount;
    private Customer customer;
    private String claimStatus;

    @Id
    @Column(name = "CLAIM_ID", nullable = false, length = 20)
    public String getClaimId() {
        return claimId;
    }

    @Column(name = "CLAIM_NUMBER", nullable = false, length = 64)
    public String getClaimNumber() {
        return claimNumber;
    }

    @Column(name = "CLAIMED_AMOUNT", nullable = true, precision = 10, scale = 2)
    public double getClaimedAmount() {
        return claimedAmount;
    }

    @Column(name = "SANCTION_AMOUNT", nullable = true, precision = 10, scale = 2)
    public double getSanctionAmount() {
        return sanctionAmount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    public Customer getCustomer() {
        return customer;
    }
    @Column(name = "STATUS_FLAG", nullable = false, length = 1)
    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public void setClaimedAmount(double claimedAmount) {
        this.claimedAmount = claimedAmount;
    }

    public void setSanctionAmount(double sanctionAmount) {
        this.sanctionAmount = sanctionAmount;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("claimId", claimId)
                .append("claimNumber", claimNumber)
                .append("claimedAmount", claimedAmount)
                .append("sanctionAmount", sanctionAmount)
                .append("customer", customer)
                .append("claimStatus", claimStatus)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Claims claims = (Claims) o;

        return new EqualsBuilder()
                .append(claimNumber, claims.claimNumber)
                .append(customer, claims.customer)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(claimNumber)
                .append(customer)
                .toHashCode();
    }
}
