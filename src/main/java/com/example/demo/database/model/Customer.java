package com.example.demo.database.model;

import com.example.demo.database.util.BooleanToStringConverter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private boolean activeFlag;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    @Id
    @Column(name = "CUSTOMER_ID", nullable = false, length = 20)
    public String getId() {
        return id;
    }

    @Column(name = "CUSTOMER_NAME", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    @Column(name = "ACTIVE_FLAG", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    public boolean isActiveFlag() {
        return activeFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("activeFlag", activeFlag)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return new EqualsBuilder()
                .append(id, customer.id)
                .append(name, customer.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(name)
                .toHashCode();
    }
}
