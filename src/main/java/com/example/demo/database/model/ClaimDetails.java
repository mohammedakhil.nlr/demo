package com.example.demo.database.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by akmohammed on 11/18/2017.
 */
@Entity
@Table(name = "CLAIM_DETAILS")
public class ClaimDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    private String claimDetailsId;
    private Claims claims;
    private String keyType;
    private String keyPath;
    private String keyStatusFlag;

    @Id
    @Column(name = "CLAIM_DETAILS_ID", nullable = false, length = 20)
    public String getClaimDetailsId() {
        return claimDetailsId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLAIM_ID")
    public Claims getClaims() {
        return claims;
    }

    @Column(name = "KEY_TYPE", nullable = false, length = 64)
    public String getKeyType() {
        return keyType;
    }

    @Column(name = "KEY_PATH", nullable = false, length = 512)
    public String getKeyPath() {
        return keyPath;
    }

    @Column(name = "STATUS_FLAG", nullable = false, length = 1)
    public String getKeyStatusFlag() {
        return keyStatusFlag;
    }

    public void setClaimDetailsId(String claimDetailsId) {
        this.claimDetailsId = claimDetailsId;
    }

    public void setClaims(Claims claims) {
        this.claims = claims;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public void setKeyPath(String keyPath) {
        this.keyPath = keyPath;
    }

    public void setKeyStatusFlag(String keyStatusFlag) {
        this.keyStatusFlag = keyStatusFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("claimDetailsId", claimDetailsId)
                .append("claims", claims)
                .append("keyType", keyType)
                .append("keyPath", keyPath)
                .append("keyStatusFlag", keyStatusFlag)
                .toString();
    }
}
