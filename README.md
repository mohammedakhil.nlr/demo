#user name password user::password

##post method
To add Customer
url:localhost:8090/demo/customer/add
example Request Body: {"id":"5","name":"Akhil","activeFlag":true}

to add claims
url:localhost:8090/demo/claims/add
example Request Body: {"claimNumber":"123456","customerId":"5","claimedAmount":"123.45","claimRequestDetails":[{"partName":"bumper","partImagePath":"c://prografiles/abc.jpeg"}]}


##get Method
to view customer
url: localhost:8090/demo/customer?id=5

to view claims using claim number and cust_id
url: localhost:8090/demo/claims?claimNumber=123456&&cust_id=5

* **A. Prerequisites**
   * 1. Windows/Linux based operating system
   * 2. Java 1.8
   * 3. Maven must be installed in the system to build and run the application

* **B. Getting Started**
   * 1. give mvn spring-boot:run from base folder of project, application will spin at post 8090
   with context path demo by default you will be prompted to login window on successful login you
   will see a welcome page